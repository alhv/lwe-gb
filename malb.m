load "lwe.m";

malb := function (c, A, F,T)
	//Malb's idea of an attack
	m := Ncols(A);
	n := Nrows(A);

	P := PRing(F,n + m);
	//unknown : m for e and n for s
	L := [];
	for i:=1 to m do
		tmp := 0;
		for j:=1 to n do
			tmp +:= P.j * A[j,i];
		end for;
		Append(~L, P.(n+i) - c[1,i] + tmp);
		Append(~L, Pol(P.(n+i), T));
	end for;
	
	return L;
end function;