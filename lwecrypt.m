// Cryptosystème présenté par O. Regev
//n le paramètre de sécurité 

Cryptosystem_Parameters:= function(n)
	p:=Random(n*n,2*n*n);
	p:=NextPrime(p);
	e:=1; //constant arbitraire >0
	//m:=Floor((1+e)*(n+1)*Log(p));
	m:=2*n;
	return n,p,m;
end function;

Private_Key :=function(n,p)
	S:=RandomMatrix(GF(p),1,n);
	return S;
end function;

Public_Key := function(n,m,p,S)
	F:=GF(p);
	e :=  Matrix(F,1,m,[Random(1) : i in [1..m]]);
	G:= RandomMatrix(F,n,m);
	b:= S*G + e;
	return G,b;
end function;

LweCrypt := function(Message,m,n,G,b,p)
	F:=GF(p);
	
	Crypt:=[];
	for i in Message do
		a:=Matrix(F, n, 1, []);
		Subset := Matrix(F,1,m,[Random(1) : i in [1..m]]);
		B:=Zero(F);
		if i eq 0 then

			for j := 1 to m do 
				
				a:=a+Subset[1,j]*ColumnSubmatrix(G, j,1);
				
				B:=B+b[1,j]*Subset[1,j];
			end for;
		else 
			for j := 1 to m do 
				a:=a+Subset[1,j]*ColumnSubmatrix(G, j,1);
				B:=B+b[1,j]*Subset[1,j];
			end for;
			B :=B+Floor(p/2);
		end if;

		Append(~Crypt,<a,B>);
	end for;
	return Crypt;
end function;

LweDecrypt :=function(Crypt,S,p)
	Decrypt:=[];
	for t in Crypt do
		tmp:=S*t[1];
		res:=t[2] -tmp[1,1];
		print "res",res;
		if res lt Floor(p/2)  then 
			
			Append(~Decrypt,0);
		else 
				Append(~Decrypt,1);
		end if;
	end for;
		return Decrypt;

end function;

Test := function()
	n,p,m:=Cryptosystem_Parameters(3);
	S:=Private_Key(n,p);
	G,b:=Public_Key(n,m,p,S);
	Message:=[Random(1) : i in [1..5]];
	print n,p,m,S;
	C:=LweCrypt(Message,m,n,G,b,p);

	A:=LweDecrypt(C,S,p);
	print Message,A,p,m;
	return 0;
end function;