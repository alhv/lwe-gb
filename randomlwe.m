SetVerbose("Faugere", 1);


PRing := function(R, n)
	P := PolynomialRing(R, n, "grevlex");
	AssignNames(~P, ["x" cat IntegerToString(i): i in [1..n]]);
	return P;
end function; 


RandomSystem := function(n,m,F)
	// Random for binary error
    P := PRing(F,n);
    L:=[];
	for i := 1 to m do
	        tmp := Random(F);
        	for j := 1 to n do
            		tmp +:= Random(F) * P.j;
            		for k := j to n do
               			 tmp +:= P.j * P.k * Random(F);
           		 end for;
        	end for;

		//For binary secret :
		//z := Evaluate(tmp,[Random(1) :i in [1..n]]);
		//tmp -:=z;
       		Append(~L,tmp);
    	end for;

    	//For binary secret :
	//for i :=1 to n do
	//	Append(~L,P.i*(P.i-1));
	//end for;
    return L,P;
end function;


RandomSystemHighDegree := function(n,m,d,F)
	// System with m equations, n variables and have a degee of d
	P := PRing(F,n);
	L:=[];
	for i := 1 to m do
		L1:=1;
		for k :=1 to d do
			tmp := Random(F);
			for j := 1 to n do
				tmp +:= Random(F)*P.j;
			end for;

			L1 *:= tmp;
		end for;

		Append(~L,L1);
	end for;
	return L,P;
end function;

Test := function()
	SetLogFile("logRandom.txt" : Overwrite := false);

	for n := 5 to 10 do
		//m:=2*n;
		a:=1.7;
		m:=Floor(n^a);

		p:=NextPrime(n*n*3000);
		L,P:=RandomSystem(n,m,GF(p));

		ResetMaximumMemoryUsage();
		tps := Realtime();
		Gb := GroebnerBasis(L : Faugere := true);
		temps:=Realtime(tps);

		print "Temps:", temps,", m:", m,", n:", n, ", memory:", GetMaximumMemoryUsage(); 
			
	end for;
	return 0;
end function ;