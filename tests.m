/* Arora - Ge */
/*
load "lwe.m";
SetLogFile("arora_n4.txt" : Overwrite :=false);
R := RealField();
sigma := 8/Sqrt(2*Pi(R));
n:=4; T := 8;
for m:=10 to 30 do
	S := [Random(NextPrime(n^2*3000)) :i in [1..n]];
	A, c, e, F:=Lwe(S,sigma,m);
	L, P := attack(c, A, F, sigma,T );
	
	tps := Cputime();
	ResetMaximumMemoryUsage();
	Gb:= GroebnerBasis(L : Faugere := true);
	temps:=Cputime(tps);
	print "Temps:", temps,", m:", m,", n:", n, ", memory:", GetMaximumMemoryUsage(), ", D:", T;
end for;
*/
/*
load "lwe.m";
SetLogFile("modulus_log.txt" : Overwrite :=false);
R := RealField();
sigma := 8/Sqrt(2*Pi(R));
n:=3; p:= 17;
S := [Random(NextPrime(1)) :i in [1..n]];
V:=[];X:=[]; T := 10;
A, c, e, F:=Lwe(S,sigma,300);
q:= #F;
p := NextPrime(300);
a,b := modswitching(c,A,q,p);
L,P:= attack(c, A, GF(q), sigma,T);
LL,PP:= attack(a, b, GF(p), sigma,T);
*/
//print T;
//V := VarietySequence(Ideal(L));
//print V;
//S;
//X;

/* Dual -> find error
SetLogFile("dual_rand_test.txt" : Overwrite := true);
R := RealField();
sigma := 8/Sqrt(2*Pi(R));
n:=8; p:= 17; m:=20;
S := [Random(NextPrime(1)) :i in [1..n]];
T := 8; B := 1000;
for m := n+1 to 40 do
	A, c, e, F:=Lwe(S,sigma,m);
	u := [];
	for j:=m-n to m-1 do
		for i in Subsets({1..m},j) do
			L := dual(c, A, F, B, [j : j in i]);
			V :=  VarietySequence(Ideal(L));
			if #V ne 0 then
			Append(~u,V[1]);
			end if;
		end for;
	end for;
	u := Matrix(u);
	print "n:",n,"m:", m,"rank:", Rank(u);
end for;
*/

/* Dual - ALHV*/
/*
load "dual.m";
SetLogFile("alhv_n4.txt" : Overwrite := true);
SetVerbose("Faugere", 1);
n:=4; 
R := RealField();
sigma := 8/Sqrt(2*Pi(R));

T := 8; 
for m := 10 to 30 do
	S := [Random(NextPrime(n^2*3000)) :i in [1..n]];
	A, c, e, F:=Lwe(S,sigma,m);

	u := KernelMatrix(Transpose(A));
	//print "rank:", Rank(u),"n:",n,"m:", m;
	L := retrieve(u,c,F,T);

	tps := Cputime();
	ResetMaximumMemoryUsage();
	Gb:= GroebnerBasis(L : Faugere := true);
	temps:=Cputime(tps);
	print "Temps:", temps,", m:", m,", n:", n, ", memory:", GetMaximumMemoryUsage(), ", D:", T;
end for;
*/

//
//V := VarietySequence(Ideal(Gb));
//V; e;
//Gb := GroebnerBasis(L);
//V := VarietySequence(Ideal(L));
/*
X := [Norm_2(V[i]) : i in [1..#V]];
Exclude(~X,0);
index := [i : i in [1..#X]];
ParallelSort(~X,~index);
u := [V[index[i]+1] : i in [1..10]]; 
u := Matrix(u);

LL := retrieve(u,c,F,T);
//sol:= VarietySequence(Ideal(LL));
*/


/* modrestriction */
/*
R := RealField();
sigma := 8/Sqrt(2*Pi(R));
m := 10; n := 5; T := 8;
S := [Random(NextPrime(n^2*3000)) :i in [1..n]];
A, c, e, F:=Lwe(S,sigma,m);
q := #F; p := 7;
cc, AA := modrestriction(c, A,p, F);
L := attack(cc, AA, F, sigma,T);
*/
//V := VarietySequence(Ideal(L));
//V;


/* MALB */
/*
load "malb.m";
//SetLogFile("malb_n4.txt": Overwrite := false);
R := RealField();
sigma := 8/Sqrt(2*Pi(R));
T := 8;
n := 3; m:=9;
//for m := 10 to 30 do
	S := [Random(NextPrime(n^2*3000)) :i in [1..n]];
	A, c, e, F:=Lwe(S,sigma,m);
	L := malb(c, A, F, T);

	tps := Cputime();
	ResetMaximumMemoryUsage();
	Gb := GroebnerBasis(L : Faugere := true);
	temps:=Cputime(tps);
	print "Temps:", temps,", m:", m,", n:", n, ", memory:", GetMaximumMemoryUsage(), ", D:", T;	
//end for;
*/


// /* SIS problem */

load "dual.m";
R := RealField();
sigma := 8/Sqrt(2*Pi(R));

// SetLogFile("log_eqrand.txt": Overwrite := false);

n := 4; m:=9;
cpt :=0;
tot:=[];
max :=200;
for j:=1 to max do
	
S := [Random(NextPrime(n^2*3000)) :i in [1..n]];
A, c, e, F:=Lwe(S,sigma,m);

q := #F;
B := Floor(q/sigma/Sqrt(m));
T := Floor(B/Sqrt(m));


// L := dual(A,F,1,10);

// Gb := GroebnerBasis(L : Faugere := true);
// V := VarietySequence(Ideal(Gb));
// W := [];
// for i:=1 to #V do 
// 	Append(~W,Norm_2(V[i]));
// end for;


//index := [1..#V];
//ParallelSort(~W,~index);

//BRUTEFORCE

// K := KernelMatrix(Transpose(A));
// W := [];
// for set in Subsequences({-T..T},m-n) do
// 	b := Matrix(F,1,m-n,set)*K;;
// 	bb := [b[1,i] : i in [1..m]];
// 	if Norm_2(bb) le B then
// 		//print "success";
// 		//print set;
// 		//print bb;
// 		Append(~W,bb);
// 		//break;
// 	end if;
// end for;
// Z := IntegerRing();
// D := [];
// cpt :=0;
// for i in W do
// 	flag := 1;
// 	for j in i do
// 		if j gt T and j lt q-T then
// 			flag:=-1;
// 			break j;
// 		end if;
// 		cpt +:=1;
// 	end for;
// 	if flag eq 1 then
// 		Append(~D,i);

// 	// for j in i do
// 	// 	k := Z ! j;
// 	// 	if k le T then
// 	// 		D[T+1+k]+:=1;
// 	// 	elif k ge q-T then
// 	// 		k:=q-k;
// 	// 		D[k]+:=1;
// 	// 	end if;
// 	// end for;
// 	end if;
// end for; 

// ALGEBRAICALLY

L := newidea(A,F,T,0,2,0);

Gb := GroebnerBasis(L);
Append(~tot,Gb);
end for;
for i in tot do
	if #i gt 1 then
		cpt+:=1;
	end if;
end for;
tot;
print "success :",cpt,"over :", max;
print "n:",n,"m:",m,"plus",(m-n)-n+1;
// if W[1] le B then
// 	print "SUCCESS";
// 	break;
// end if;
//end for;

/*
load "dual.m";
R := RealField();
sigma := 8/Sqrt(2*Pi(R));
T := 4;
n := 5; m:=6;
S := [Random(NextPrime(n^2*3000)) :i in [1..n]];
A, c, e, F:=Lwe(S,sigma,m);
L := dual(A,F,T);

Gb := GroebnerBasis(L);
V := VarietySequence(Ideal(Gb));
q := #V;
W:= [Norm_2(V[i]) : i in [1..q]];
index := [1..q];
ParallelSort(~W,~index);

Z := IntegerRing();
Wc:=[]; We := [];
for i:=2 to q do
	tmp :=0;
	tmp2 :=0;
	k:=index[i];
	for j:=1 to m do
		//tmp +:= (Z ! c[1,j])* (Z ! V[k,j]);	
		tmp2 +:= (Z ! e[1,j])* (Z ! V[k,j]);	
	end for;
	//Append(~Wc,tmp);
	Append(~We,tmp2);
end for;
*/


// load "estimator.m";

// n:=256;
// q:=2^15;
// alpha := 8/q;
// R := RealField();
// sigma := 8/Sqrt(2*Pi(R));
// L := [];
// old_T:=1;
// tmp:=0;
// for i in [9..310] do
// 	m:=2^i;
// 	B := Floor(q/sigma/Sqrt(m));
// 	T := Floor(B/Sqrt(m));
	
// 	if T eq 0 then
// 		T:=1;
// 	end if;
// 	//if old_T ne T or tmp ne -1 then
// 		tmp :=DegreeOfSis(n,m,T,0,0,512);
// 		Append(~L,tmp);
// 	//end if;
// 	print i,tmp;
// 	old_T := T;
// end for;


// for i in [9..300] do
// 	m:=2^i;
// 	B := Floor(q/sigma/Sqrt(m));
// 	T := Floor(B/Sqrt(m));
// 	if T eq 0 then
// 		T:=1;
// 	end if;
// 	k:=4*n;
// 	tmp:=DegreeOfSis(n,m,T,0,0,k);
// 	while tmp eq -1 do
// 		k+:=n;
// 		tmp:=DegreeOfSis(n,m,T,0,0,k);
// 	end while;
// 	print m,tmp;
// 	Append(~L,tmp);
// end for;

// load "estimator.m";

// n:= 256;
// q := 2^15;
// alpha := 8/q;
// R := RealField();
// sigma := 8/Sqrt(2*Pi(R));

// SetLogFile("log_deg.txt": Overwrite := false);
// k:=0;

// for i in [9..300] do
// 	m:=2^i;
// 	k:=2*k;
// 	B := Floor(q/sigma/Sqrt(m));
// 	T := Floor(B/Sqrt(m));
// 	if T eq 0 then
// 		T:=1;
// 	end if;

// 	degree := DegreeOfSis(n,m,T,k,0,256);

// 	while degree eq -1 do
// 		k+:=1;
// 		B := Floor(q/sigma/Sqrt(m));
// 		T := Floor(B/Sqrt(m));
// 		if T eq 0 then
// 			T:=1;
// 		end if;
// 		degree := DegreeOfSis(n,m,T,k,0,256);
// 	end while;
// 	print i,k,degree;
// end for;