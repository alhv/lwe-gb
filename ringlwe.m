SetVerbose("Faugere", 1);

PRingQuo := function(R, n)
	P<x> := PolynomialRing(R);
	f:=x^n + 1;
	a<x>:= quo<P|f>;

	return a;
end function;

PRing := function(R, n)
	P := PolynomialRing(R, n, "grevlex");
	AssignNames(~P, ["x" cat IntegerToString(i): i in [1..n]]);
	return P;
end function; 

Pol := function(X)
	return X*(X-1);
end function;


Rlwe := function(S)
	n:=#S;
	p:= NextPrime(3000*n^2);

	R := PRingQuo(GF(p),n);
	c := Random(R);

	e := elt<R | [Random(1) : i in [1..n]]>;

	s := elt<R | S>;
	return c, c*s+e, p, n,e;
end function;



attack := function(c1,c2, p,n,S)

	P := PRing(GF(p),n);
	Rp := PRingQuo(P,n);
	
	c := elt<Rp | [Coefficient(c1,i) : i in [0..n-1]]>;
	s := elt< Rp | [P.i : i in [1..n]]>; 
	
	Sx := c*s;

	L:=[];
	for i := 0 to n-1 do
		Append(~L,Pol( Coefficient(c2,i) - Coefficient(Sx,i) ));
	end for;
	// For oracle :
	//Append(~L,P.1-S[1]);
	return L, P,Sx;
end function;


Test := function()
	SetLogFile("logRlwe.txt" : Overwrite := true);

	for taille := 3 to 10 do
		S:= [Random(taille^2*300) : i in [1..taille]];
		c1,c2,p,n := Rlwe(S);
		L,P := attack(c1,c2,p,n,S);

		tps := Cputime();
		ResetMaximumMemoryUsage();
		Gb := GroebnerBasis(L : Faugere := true);
		temps := Cputime(tps);
		print "Temps:", temps, ",  n:", taille, ", memory:", GetMaximumMemoryUsage(), ", D:", D;	
	end for;
	return 0;
end function;
