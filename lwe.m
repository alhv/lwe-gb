 SetVerbose("Faugere", 1);


ShiftRows := function(v);
	size:=NumberOfColumns(v);
	v1:=v;
	for i :=1 to size-1 do
		v1[1,i+1]:=v[1,i];

	end for;
	v1[1,1]:=v[1,size];
	return v1;
end function;

RandomCirculantMatrix := function(F,n,m)
	
	v := RandomMatrix(F,1,m);
	Set := [];
	Append(~Set,v);
	for i := 2 to n do
		v:=ShiftRows(v);
		Append(~Set,v);
	end for;
	
	return VerticalJoin(Set);
end function;

// For Discret Gaussian distribution :
load "gauss.m";

Lwe := function(S,D,m)

	n:=#S;
	//m:= Random(n*Floor(1+1/(2*Log(n))),n*n); 

	// For m = n ^ alpha :
	//alpha := 1.3
	//m :=Floor( n^alpha);

	p:=NextPrime(2^8);
	F:=GF(p);
	S := Matrix(F, 1,n,S);

	if D eq 0 then
		e := Matrix(F,1,m,[Random(1) : i in [1..m]]);
	else
		y:=Gauss_Discret(D,10^(-50));
		L:=Cumulative_GaussDiscret(y);	

		// Need the file gauss for the SampleZm function
		e :=  Matrix(F,1,m,[Random_GaussDiscret(L) : i in [1..m]]);
	end if;
	//print e;
	
	A := RandomMatrix(F,n,m);
	//For binary A :
	// A := RandomMatrix(GF(2),n,m);
	//For circulant matrix A :
	// A := RandomCirculantMatrix(F,n,m);

	c := S*A + e;
	return A, c, e, F;
end function;

LweInv := function(c, A, e)
	c := c-e;	
	return Solution(A,c);
end function;

PRing := function(R, n)
	P := PolynomialRing(R, n,"grevlex");
	AssignNames(~P, ["x" cat IntegerToString(i): i in [1..n]]);
	return P;
end function; 


Pol := function(X, D)
	if D eq 0 then		
		return X*(X-1);
	end if;
	p := X;
	for i := 1 to Floor(D) do
		p *:=(X-i)*(X+i);
	end for;
	return p;
end function;

modswitching := function(c, A, p, q)
	m := Ncols(A);
	n := Nrows(A);
	Z := IntegerRing();
	R := RealField();
	F := GF(p);
	cc:=[];
	GG:=[];
	for i:=1 to m do
		tmp := R ! Z  ! c[1][i] ;
		Append(~cc,F ! Round(p/q*tmp) );
	end for;
	for i:=1 to n do
		L:=[];
		for j := 1 to m do
			Append(~L,Round(p/q * (R ! Z ! A[i][j])));
		end for; 
		Append(~GG,L);
	end for;
	GG := Matrix(F,n,m,GG);
	cc := Matrix(F,1,m,cc);
	return cc, GG;
end function;

modrestriction := function (c, A, p ,F)
		m := Ncols(A);
		n := Nrows(A);

		Z := IntegerRing();
		//F := GF(q);
		cc := [];
		AA := [];
		for i := 1 to n do
			for j:=1 to m do
				Append(~AA,( Z ! A[i,j]) mod p);
			end for;
		end for;
		for i :=1 to m do
			Append(~cc,(Z ! c[1,i]) mod p);
		end for;

		cc := Matrix(F,1,m,cc);
		AA := Matrix(F,n,m,AA);
		return cc,AA;
end function;


attack := function(c, A, F, D, T,...)
	/*
	T is a list of the degrees for the polynomials to
	be distributed according to the distribution
	Return a list of polynomial over F[x₁,...,xₙ] according
	to Arora-Ge
	*/

	m := Ncols(A);
	n := Nrows(A);

	P := PRing(F,n);
	L := [];

	y:=Gauss_Discret(D,10^(-50));
	C:=Cumulative_GaussDiscret(y);

	for i := 1 to m do
		tmp := 0;
		for j := 1 to n do
			tmp +:= P.j * A[j,i];
		end for;
		L1 := c[1,i] - tmp;
		TRandom := Random_Polynomial(C,T);
		Append(~L,Pol(L1,TRandom));
	end for;

	// For binary secret :
	/*
	for i := 1 to n do
		Append(~L,Pol(P.i,0));
	end for;
	*/
	return L, P;
end function;