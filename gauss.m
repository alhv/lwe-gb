// We want to generate a Gaussian distribution


T := function(n,t,P)
	//P for precision, need to be really small
	res:=0;
	tmp:=1/Factorial(n)*(t/2)^n;
	m:=1;
	while (Abs(res-tmp) gt P) do
		res := tmp;
		tmp +:= 1/(Factorial(m)*Factorial(m+n))*(t/2)^(2*m+n);
		m+:=1;
	end while;

	return res*Exp(-t);
end function;

Gauss_Discret := function(t,P)
	//P for precision, need to be really small
	i:=0;
	y:=[];
	tmp:=T(0,t,P);
	while tmp gt P do
		Append(~y,tmp);
		i+:=1;
		tmp := T(i,t,P);
	end while;

	return y;
end function;

Cumulative_GaussDiscret := function(y)
	n := #y;
	L:=[];
	Append(~L,y[n]);
	i := n-1;
	while i gt 1 do 
		Append(~L,y[i] + L[n-i]);
		i-:=1;
	end while;

	for i:= 1 to n do
		Append(~L,y[i] + L[n+i-2]);
	end for;
	return L;

end function;



Random_GaussDiscret := function(L)
	P:= 10^50;
	R := RealField();
	x := R ! Random(P)/P;
	n := (#L-1)/2;

	for i := 1 to #L do
		if x le L[i] then
			//print L[i-1], x, L[i];
			return i-n;
		end if;
	end for;

	return #L+1-n;
end function;

Interval_Probability := function(L,T)
	n := Floor((#L-1)/2);
	return L[n + T] - L[n - T - 1];
end function;

Random_Polynomial := function(L,T)
		n := #T ;
		dist:= [];

		if n eq 1 then
			return T[1];
		end if;

		for i:= 1 to n do;
			Append(~dist, Interval_Probability(L,T[i]));
		end for;

		P:= 10^50;
		R := RealField();
		x := R ! Random(P)/P;
		//print dist;

		index := [i : i in [1..n]];
		ParallelSort(~dist,~index);

		for i := 1 to n-1 do
			if x le dist[i] then
				t := index[i];
				return T[t]; 
			end if;
		end for;

		t := index[n];
		return T[t];
		
end function;

Test := function(s)
	//s the variance

	y:=Gauss_Discret(s,10^(-50));
	L := Cumulative_GaussDiscret(y);

	n:=(#L-1)/2;
	hist := [0 : i in [1..2*Floor(n)+1]];
	print hist;
	for i:=1 to 100000 do
		k:= n+Random_GaussDiscret(L);
		hist[Floor(k)]+:=1;
	end for;
	return hist;
end function;


/*
y:= Gauss_Discret(3,10^(-50));
L := Cumulative_GaussDiscret(y);

test := [0 : i in [1..7]];
print test;
for i:=1 to 10000 do
	r := Random_Polynomial(L,[*1,2,3,4,5,6,7*]);
	test[r]+:=1;
end for;
print test;
*/

