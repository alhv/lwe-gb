Attack on LWE using Gröbner basis
=================================

This is a master 1 research project on solving `Learning with Errors <https://en.wikipedia.org/wiki/Learning_with_errors>`__ using `Gröbner basis <https://en.wikipedia.org/wiki/Gr%C3%B6bner_basis>`__ on the Arora-Ge algorithm. We use `MAGMA <http://magma.maths.usyd.edu.au/magma/>`__ .

In this work we wanted to know if it was possible to improve solving LWE [#]_ using the Arora-Ge algorithm [#]_ with Gr\"{o}bner Basis [#]_ by modeling the error following the gaussian distribution as two uniform distributions and not one. This led to a failure, so we tried to find an algebraic equivalent to the dual attack [#]_. At first we thought that this would be a promising idea asymptotically but the system to solve is too difficult.

.. [#]	O. Regev, “On lattices, learning with errors, random linear codes, and cryptography,” in Proc. 37th ACM Symp. on Theory of Computing (STOC),2005, pp. 84–93.
.. [#]	S. Arora and R. Ge, “New algorithms for learning in presence of errors,” in Automata, Languages and Programming: 38th International Colloquium, ICALP 2011, Zurich, Switzerland, July 4-8, 2011, Proceedings, Part I, L. Aceto, M. Henzinger, and J. Sgall, Eds. Berlin, Heidelberg: Springer Berlin Heidelberg, 2011, pp. 403–415.
.. [#]	M.R. Albrecht, C. Cid, J.-C. Faugère, R. Fitzpatrick, and L. Perret, “Algebraic algorithms for lwe problems,” ACM Commun. Comput. Algebra, vol. 49, no. 2, pp. 62–62, Aug. 2015.
.. [#]	M.R. Albrecht, C. Cid, J.-C. Faugère, R. Fitzpatrick, and L. Perret, “On the complexity of the bkw algorithm on lwe,” Designs, Codes and Cryptography, vol. 74, no. 2, pp. 325–354, Feb. 2015.