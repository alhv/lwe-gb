DegreeOfRegularity := function(n,D : precision:=512)
    /*
    param D: tuple of (d,m) where m is the number of equations of
    degree d
    */
    if n le 64 then
        R<z> := PowerSeriesRing(RationalField(),200);
    elif n le 512 then
        R<z> := PowerSeriesRing(RationalField(),2*n);
    else
        R<z> := PowerSeriesRing(RationalField(),precision);        
    end if;
    s := &* [(1-z^d[1])^d[2] : d in D] / (1-z)^n;
    dreg := 0;
    for i in Coefficients(s) do
        if i le 0 then 
            return dreg;
        end if;
        dreg +:= 1;
    end for;
    return -1;
end function;


ps_single := function(C : R := RealField(200))
    return 1 - (2/(C*Sqrt(2*Pi(R))) * Exp(-C^2/2));
end function;


preprocess := function(n,alpha,q,probability, omega : R:= RealField(200))
    /*
    Check if parameters n, α, q, probability and ω are sound and return correct types
    */
    if probability ge 1 or probability le 0 then
        error Error("Probability should be in ]0,1[");
    elif n lt 1 then
        error Error("LWE dimension must be greater than 0.");
    elif alpha lt 0 then
        error Error("Fraction of noise must be > 0.");
    elif q lt 1 then
        error Error("LWE modulus must be greater than 0.");
    elif omega lt 2 or omega ge 3 then
        error Error("ω must be in [2,3).");
    end if;

    alpha := R ! alpha;
    q := R ! q;
    probability := R ! probability;
    return n,alpha,q,probability;
end function;

arora_gb := function(n,alpha,q : probability := 0.99, omega := 2, precision:=500)
    /*
    Arora-GB as described in [AroGe11,ACFP14]_

    :param n: LWE dimension `n > 0`
    :param alpha: noise rate `0 ≤ α < 1`, noise will have standard deviation `αq/√{2π}`
    :param q: modulus `0 < q`
    :param success_probability: targeted success probability < 1
    :param omega: linear algebra constant 2 ≤ ω < 3

    ..  [ACFP14] Albrecht, M.  R., Cid, C., Jean-Charles Faug\`ere, & Perret, L.  (2014).
        Algebraic algorithms for LWE.

    ..  [AroGe11] Arora, S., & Ge, R.  (2011).  New algorithms for learning in presence of
        errors.  In L.  Aceto, M.  Henzinger, & J.  Sgall, ICALP 2011, Part~I (pp.  403–415).  :
        Springer, Heidelberg.
    */

    R := RealField(precision);

    try
        n, alpha, q, probability := preprocess(n,alpha,q,probability, omega :R:=R);
    catch e
        error "Error with parameter: ", e`Object;
    end try;

    stddev := alpha*q/Sqrt(2*Pi(R));

    m_best := 0;
    d_best :=0;
    t_best := 0;
    t2_best := 0;
    stuck := 0;

    for t := Floor((n-1)/6) to n do
        d := 2*t +1;
        C := t/stddev;
    
        if C ge 1 then
            try
                m_req := Log(probability) / Log(ps_single(C : R:=R)) ;          
            catch e
                print "Have to increase the float precision";
                return $$(n,alpha,q : probability := probability, omega := omega, precision :=2*precision);
            end try;

            m_sub := Floor(m_req);

            for j := 10 to Ceiling(Log(2,m_sub)) do
                m_req:=2^j;
            

            for t2 := Floor((n-1)/6) to t-1 do
                d2:=2*t+1;
                C2 := t2/stddev;
                
                m2:= (Log(R ! 0.8) - m_req * Log(ps_single(C : R:=R)))/Log(ps_single(C2 : R:=R));


                if m_req gt n then
                    current := DegreeOfRegularity(n,[<d,m_req>,<d2,m2>]);
                    //print t2, current;
                    if current ne -1 then
                        //print current;
                    if d_best eq 0 then
                        d_best := current;
                        m_best := m_req;
                        d_best := current;
                        t_best := t;
                        t2_best := t2;
                        stuck :=0;
                        
                    else
                        if d_best gt current then
                            m_best := m_req;
                            d_best := current;
                            t_best := t;
                            t2_best := t2;
                            stuck := 0;
                        else
                            stuck +:=1;
                            if stuck ge 10 then
                                break t2;
                            end if;
                        end if;
                    end if;
                    end if;
                end if;
            end for;
            end for;
            print t_best,t2_best ,d_best;
        end if;
    end for;
    m_log := Log(2,m_best);
    // For a precision of 1 ⇒ printing 
    RR := RealField(Ceiling(Log(10,m_log))+1);

    print "Log₂rop : ",RR ! Log(2,Binomial(n+d_best,d_best)^omega);

    print "Log₂mem : ", RR ! Log(2,Binomial(n+d_best,d_best)^2);
    print "Log₂m Dreg T :";
    return RR ! m_log,d_best,t_best;
end function;

DegreeOfArora := function (n,m,T)
    return DegreeOfRegularity(n,[<2*T+1,m>]);
end function;

DegreeOfMalb := function (n,m,T)
    return DegreeOfRegularity(n+m,[<2*T+1,m>,<1,m>]);
end function;

DegreeOfAlhv := function (n,m,T)
    return  DegreeOfRegularity(m,[<2*T+1,m>,<1,m-n>]);    
end function;

DegreeOfSis := function (n,m,T,k,l, precision)
    // k random linear equation, l randomly picked variable
    return  DegreeOfRegularity(m-n-k,[<2*T+1,m-k-l>,<1,l>] : precision:=precision);
end function;


preprocess_sis := function(n,alpha,q,R)
    /*
    Check if parameters n, α, q, probability and ω are sound and return correct types
    */
    if n lt 1 then
        error Error("LWE dimension must be greater than 0.");
    elif alpha lt 0 then
        error Error("Fraction of noise must be > 0.");
    elif q lt 1 then
        error Error("LWE modulus must be greater than 0.");
    end if;

    alpha := R ! alpha;
    q := R ! q;
    return n,alpha,q;
end function;

find_bound := func<q,sigma,m |Floor(q/sigma/Sqrt(m)) >;

sis_gb := function (n, alpha, q : precision := 20)
    R := RealField(precision);
    try
        n, alpha, q:= preprocess_sis(n,alpha,q,R);
    catch e
        error "Error with parameter: ", e`Object;
    end try;

    sigma :=alpha*q/Sqrt(2*Pi(R));
    d_best :=0;
    m_best := 0;
    t_best := 0;
    stuck := 0;
    for k in [Floor(Log(2,2*n))..300] do
        m:= 2^k;
        B := find_bound(q,sigma,m);
    
        T := Floor(B/Sqrt(m));
        if T eq 0 then
            T:=1;
        end if;
        Deg := DegreeOfSis(n,m,T,m-2*n,0);
        print k,Deg,T;
        if d_best eq 0 then
            d_best := Deg;
            m_best :=m;
            t_best := T;
        else
            if Deg le d_best then
                stuck :=0;
                d_best := Deg;
                m_best :=m;
                t_best := T;
            else
                //stuck +:=1;
                if stuck ge 5 then
                    print m;
                    break;
                end if;
            end if;
        end if;
    end for;

    m_log := Log(2,m_best);
    RR := RealField(Ceiling(Log(10,m_log))+1);
    print "Log₂m Dreg T :";
    return RR ! m_log,d_best,t_best;
end function;

R:= RealField();
n:=256;
q:=2^15;
alpha := 8/q;
sigma :=alpha*q/Sqrt(2*Pi(R));


find_T := function (p,m)
    R := RealField(500);
    RX<x> := PolynomialRing(R);
    return Roots(Log(Sqrt(Pi(R)/2)*(1-R !p^(1/m)))+x^2/2 + 2);
end function;

// List1 := []; T:=65;
// for m in [2*n..n^2] do
//     Append(~List1,DegreeOfArora(n,m,T));
//     print m;
// end for;


// L:=[];
// for m in [Log(2,2*n)..300] do
//     B := Round(q/sigma/Sqrt(2^m));
//     T := Round(B/Sqrt(2^m));
//     Append(~L,T);
// end for;

