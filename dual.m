load "lwe.m";


Norm_2 := function(S)
	tmp := 0;
	Z := IntegerRing();
	q := #Parent(S[1]);
	W :=[];
	for i in S do
		if i ge Floor(q/2) then
			Append(~W,Z! (-i));
		else
			Append(~W, Z ! i);
		end if;

	end for;

	for i in W do
		tmp +:= Z ! (i^2);
	end for;
	return Floor(Sqrt(tmp));
end function;

Pol_2 := function(X, D)
		k:=Sign(D);
		p := X-1*k;
        for i := 2 to Floor(D) do
                p *:=(X-i*k);
        end for;
        return p;
end function;


dual := function(A, F, k, T)
	m := Ncols(A);
	n := Nrows(A);

	P := PRing(F,m);
	L:= [];	
	tmp := 0;
	for i := 1 to n do
		for j := 1 to m do
			tmp +:= P.j *A[i,j];
		end for;
		Append(~L,tmp);
	end for;

	// for i:= n+1 to m-k do
	// 	tmp :=Random(F);
	// 	for j:=n+1 to m-1 do
	// 		tmp+:= Random(F)*P.j;
	// 	end for;
	// 	Append(~L,tmp);
	// end for;


	 for i:=1 to m do
	 	Append(~L,Pol(P.i,T));
	 end for;

	return L;
end function;


retrieve := function(u,c,F,T)
	m := Nrows(u);
	n := Ncols(u);
	P := PRing(F,n);
	L:=[];
	eu := [(c*Matrix(n,1,u[i]))[1,1]: i in [1..m]];

	for i:= 1 to m do
		tmp := 0;
		for j := 1 to n do
			tmp+:=P.j*u[i,j];
		end for;
		Append(~L,tmp-eu[i]);
	end for;
	
	for i := 1 to n do
		Append(~L,Pol(P.i,T));
	end for;

	return L;
end function;

newidea := function (A,F,T,h,k,l)
	// l : only postive of negative
	// k : randomly assigned
	// h : random equation in kernel
	A := Transpose(A);
	U := KernelMatrix(A);

	m := Nrows(A);
	n := Ncols(A);

	P := PRing(F,m);
	L := [];

	// |αᵢ| ≤ T
	for i:=k+l+1 to m do
		Append(~L,Pol(P.i,T));
	end for;

	for i:=k+1 to k+l do
		Append(~L,Pol_2(P.i,(-1)^i*T));
	end for;

	// uᵢ = ΣαⱼUⱼᵢ = -Σu_j+n Uⱼᵢ 
	for i:=1 to n do
		tmp:=P.i;
		for  j:=1 to m-n do
		 	tmp +:=P.(n+j)*U[j,i];
		 end for; 
		 Append(~L,tmp);
	end for; 	
	
	for i:=1 to k do
		tmp:=Random(F);
		Append(~L,P.i-Random(-T,T));
	end for;

	for i:= 1 to h do
		tmp:=Random(-T,T);
		for j:=n+1 to m do
			tmp +:=P.(j)*Random(-T,T);
		end for;
		Append(~L,tmp);
	end for;

	return L;
end function;