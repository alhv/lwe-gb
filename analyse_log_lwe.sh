#!/bin/bash
grep 'FAUGERE|step degree: [0-9]*|No pairs to reduce|No new|Temps: [0-9.]* , m: [0-9]* , n: [0-9]* , memory: [0-9]*' $1 -o -E | awk 'NR==1 {flag=-1; max=tmp=0}; $1=="Temps:"{printf("%i %i %.3f %.0f %i\n",$8,$5,$2,$11,max) ; max=tmp=0; flag=-1};  $1=="FAUGERE" {flag=flag+1}; $1=="No" {max=tmp} ;$1!="FAUGERE" && $1!="No" && $1!="Temps:" {tmp=max;max=(flag==0)?(max>$3?max:$3):max}'  > $2
